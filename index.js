const express = require("express");
const chalk = require("chalk");
const app = express();

app.use(express.json()); // set up app JSON

//set up port
const port = 900;
app.listen(port, () => {
    console.log(`run port ${port}`);
});

let listMovie = [
    {
        id: "1",
        name: "Lật mặt 5",
        totalMovietime: "20h",
        poster: "/imgs/latmat.jpg",
        trailer: "/youtube/trailer/latmat"
    },

    {
        id: "2",
        name: "Iron Man",
        totalMovietime: "16h",
        poster: "/imgs/Ironman.jpg",
        trailer: "/youtube/trailer/ironman"
    },

    {
        id: "3",
        name: "Em là bà nội của anh",
        totalMovietime: "21h",
        poster: "/imgs/emlabanoicuaanh.jpg",
        trailer: "/youtube/trailer/emlabanoicuaanh"
    },
]

// Lấy danh sách movie
app.get("/api/movie", (req, res) => {
    res.status(200).send(listMovie);
})

// Lấy chi tiết movie
app.get("/api/movie/:id", (req, res) => {
    // lấy id movie của người dùng nhập vào
    const detailMovie = listMovie.find((item) => item.id == req.params.id);
    if (detailMovie) {
        res.status(200).send(detailMovie);
    } else {
        res.status(404).send("Get Not Found!");
    }
});

// thêm movie vào danh sách movie
app.post("/api/movie", (req, res) => {
    const data = req.body
    const { id, name, totalMovietime, poster, trailer } = req.body;
    const newMovie = { id, name, totalMovietime, poster, trailer };
    console.log(data);
    listMovie = [...listMovie, newMovie];
    res.status(200).send(listMovie);
    console.log(chalk.red("add success!"));
});
// xóa movie
app.delete("/api/movie/:id", (req, res) => {
    const index = listMovie.findIndex((item) => item.id == req.params.id);
    if (index !== -1) {
        const deleteMovie = listMovie[index];
        listMovie.splice(index, 1);
        res.status(200).send(deleteMovie);
    } else {
        console.log(chalk.red("delete success!"));
        res.status(404).send("delete success!");
    }
})
//update movie
app.put("api/movie/:id", (req, res) => {
    const { id } = res.params;
    const { id, name, totalMovietime, poster, trailer } = req.body;
    const index = listMovie.findIndex((item) => item.id == id);
    if (index !== -1) {
        const movie = listMovie[index];
        const updateMovie = { ...movie, id, name, totalMovietime, poster, trailer }
        listMovie[index] = updateMovie;
        res.status(200).send(updateMovie);
    } else {
        res.status(404).send("Not found");
    }
})